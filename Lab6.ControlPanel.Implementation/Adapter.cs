﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lab6.ControlPanel.Contract;
using System.Windows;
using Lab6.MainComponent.Contract;

namespace Lab6.ControlPanel.Implementation
{
    public class Adapter : IControlPanel
    {
        wyswietlacz window;
        public Adapter(IKawa Kawa)
        {
            this.window = new wyswietlacz(Kawa);
        }
        public Window Window
        {
            get { return window; }
        }

    }
}
