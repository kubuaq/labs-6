﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lab6.MainComponent.Contract;
using Lab6.Display.Contract;

namespace Lab6.MainComponent.Implementation
{
    public class Kawa: IKawa
    {
        IDisplay display;
        public Kawa(IDisplay display)
        {
            this.display = display;
        }
        public Kawa()
        {
        }
        public void late()
        {
            Console.WriteLine("zrobiles late");
        }
        public void kapucino()
        {
            Console.WriteLine("zrobiles kapucino");
        }
        public void makiato()
        {
            Console.WriteLine("zrobiles makiato");
        }
    }
}
