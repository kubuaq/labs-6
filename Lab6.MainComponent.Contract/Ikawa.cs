﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab6.MainComponent.Contract
{
    public interface IKawa
    {
        void kapucino();
        void makiato();
        void late();
    }
}
